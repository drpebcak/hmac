// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/base64"
	"fmt"

	"crypto/hmac"
	"crypto/sha1"
	"net/url"
	"strings"
	"time"

	"errors"
	"os"

	"github.com/spf13/cobra"
)

var authHeader, timHeader string

// generateCmd represents the generate command
var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "generate hmac headers",
	Long:  `Pass in hmac key/secret and date, will generate authorization header`,
	Run: func(cmd *cobra.Command, args []string) {
		if *hmacKey == "" && *hmacSecret == "" {
			fmt.Println(errors.New("HMAC credentials are required here..."))
			os.Exit(1)
		} else {
			generateHmac()
		}

		fmt.Printf("Date: \"%s\"\n", timHeader)
		fmt.Printf("Authorization: \"%s\"\n", authHeader)
	},
}

func init() {
	RootCmd.AddCommand(generateCmd)
}

func generateHmac() {
	if *hmacKey == "" && *hmacSecret == "" {
		fmt.Println("Nothing to see here...")
		fmt.Println()
	} else {
		refDate := "Mon, 02 Jan 2006 15:04:05 UTC"
		loc, _ := time.LoadLocation("UTC")
		tim := time.Now().In(loc).Format(refDate)
		//req.Header.Add("Date", tim)
		// Prepare the signature to include those headers:
		signatureString := strings.ToLower("Date") + ": " + tim

		// SHA1 Encode the signature
		HmacSecret := *hmacSecret
		key := []byte(HmacSecret)
		h := hmac.New(sha1.New, key)
		h.Write([]byte(signatureString))
		KeyID := *hmacKey

		// Base64 and URL Encode the string
		sigString := base64.StdEncoding.EncodeToString(h.Sum(nil))
		encodedString := url.QueryEscape(sigString)
		timHeader = tim
		authHeader = fmt.Sprintf("Signature keyId=\"%s\",algorithm=\"hmac-sha1\",signature=\"%s\"", KeyID, encodedString)
		// Add the header
		//req.Header.Add("Authorization", fmt.Sprintf("Signature keyId=\"%s\",algorithm=\"hmac-sha1\",signature=\"%s\"", KeyID, encodedString))
	}
}
