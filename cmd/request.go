// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"strings"

	"net/http"

	"fmt"

	"errors"

	"github.com/spf13/cobra"
)

var requestUrl, requestData, requestMethod *string
var showHeaders, prettyPrint *bool

// requestCmd represents the request command
var requestCmd = &cobra.Command{
	Use:   "request",
	Short: "Sends a request with HMAC headers",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		if *requestUrl == "" {
			fmt.Println(errors.New("URL is required (-u)"))
			os.Exit(1)
		}
		if *hmacKey == "" && *hmacSecret == "" {
			fmt.Println("You didn't provide hmac credentials, but I'll send this request anyway...")
			fmt.Println()
		} else {
			generateHmac()
		}

		if *showHeaders {
			fmt.Println("----- Headers -----")
			fmt.Println("Date Header:", timHeader)
			fmt.Println("Authorization Header:", authHeader)
			fmt.Println()
		}

		body := strings.NewReader(*requestData)
		req, _ := http.NewRequest(*requestMethod, *requestUrl, body)
		client := &http.Client{}

		req.Header.Add("Date", timHeader)
		req.Header.Add("Authorization", authHeader)
		if resp, err := client.Do(req); err != nil {
			panic(err)
		} else {
			defer resp.Body.Close()
			fmt.Println("----- Status Code -----")
			fmt.Println(resp.StatusCode)
			fmt.Println()
			fmt.Println("----- Response -----")
			fmt.Println()

			output, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				panic(err)
			}
			if *prettyPrint {
				var m interface{}
				json.Unmarshal(output, &m)
				response, err := json.MarshalIndent(m, "", "  ")
				if err != nil {
					fmt.Println("Error, could not pretty print!")
					fmt.Println()
					fmt.Println(string(output))
				} else {
					fmt.Println(string(response))
				}
			} else {
				fmt.Println(string(output))
			}
		}
	},
}

func init() {
	RootCmd.AddCommand(requestCmd)
	requestUrl = requestCmd.Flags().StringP("url", "u", "", "Request URL")
	requestData = requestCmd.Flags().StringP("data", "d", "", "Request Data")
	requestMethod = requestCmd.Flags().StringP("method", "X", "GET", "Request Method")
	prettyPrint = requestCmd.Flags().BoolP("pretty", "p", false, "Pretty Print json response")
	showHeaders = requestCmd.Flags().BoolP("generate", "g", false, "Prints out generated headers")
}
